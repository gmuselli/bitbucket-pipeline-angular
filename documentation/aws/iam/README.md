# IAM

## Prerequesite

- You need first to create an aws account. (if it is not the case go to [this page](https://aws.amazon.com/premiumsupport/knowledge-center/create-and-activate-aws-account/))
- Then go to [IAM home](https://console.aws.amazon.com/iam/home#/home). IAM stands for Identity Access Management. It helps you to create groups/users and assign them the correct access to your AWS ecosystem. It can be for Applications (like S3, EC2, ECR...) but as well for administrative tasks (like Billing).

## Create Group

You need first to create a group. To do this click on "Create group". You should see the following page. Enter a name.

![Create group](./IAM_Create_Group.png)

Click then on "Next step". You will see the most important page of IAM. This is the list of available policies. You can create yours but there are a lot of predefined one. So you can simply select the one we need. Here it is "AmazonS3FullAccess". 

*In fact we could reduce the rights as we will only need "PutObject" Permission. But let's keep it like this for the moment. I invite you to have a look to the IAM documentation to really adjust the right you need for each user.*

![Create group Policy](./IAM_Create_Group_Policy.png)

To complete go to "Next" and then "Create User"

## Create User

You need then to create a user. To do this click on "Add user". You should see the following page. Enter a name and select "Programmatic access".

*This will indicate that the current user won't be able to access to the console but will only be able to work with the cli and the api*

![Create user](./IAM_Create_User.png)

Once you clicked on "Next Permissions". You should see the following page:

![Create user](./IAM_Create_User_Assign_Group.png)

You need then to assign it to the group we just created. Once you clicked on "Next Tags" you should see a page that let you add Tags to identify easily your user. Go until "Create user"

## Generate User key

You need then to generate a User key. To do this click on Users and then on the user you just created. Then on the tab "Security credentials" you should see a button "Create access key". Click on it and save the Access Key somewhere (safe) as you won't be able to see the Secret after.

![Create user](./IAM_Create_User_Key.png)