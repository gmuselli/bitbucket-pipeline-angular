#!/bin/bash

# This script is there to check if everything has been well configured

# $1: The variable name
# $2: The variable value
function checkVariableDefined() {
    VARIABLE_NAME=$1
    VARIABLE_VALUE=$2

    if [ -z "${VARIABLE_VALUE}" ]; then
        echo "${VARIABLE_NAME} is not defined. Please have a look to the global variable in Settins/Pipelines/Repository variables."
        exit 1
    fi
}

checkVariableDefined AWS_REGION ${AWS_REGION}
checkVariableDefined AWS_ACCESS_KEY ${AWS_ACCESS_KEY}
checkVariableDefined AWS_SECRET_ACCESS_KEY ${AWS_SECRET_ACCESS_KEY}
checkVariableDefined AWS_BUCKET_ARTIFACT ${AWS_BUCKET_ARTIFACT}
checkVariableDefined AWS_BUCKET_PROD_DEPLOY ${AWS_BUCKET_PROD_DEPLOY}
checkVariableDefined AWS_BUCKET_TEST_DEPLOY ${AWS_BUCKET_TEST_DEPLOY}

if [ ! -z "${RELEASE_PATTERN}" ]; then

    (echo "${RELEASE_PATTERN}" | grep -Eq ^[\-_a-zA-Z0-9/]*[\*]$) && echo "Pattern \"${RELEASE_PATTERN}\" OK" || echo "Pattern for RELEASE_PATTERN must match ^[\-_a-zA-Z0-9/]*[\*]$ like release/* or production/*"

elif [ -z "${PRODUCTION_BRANCH}" ]; then
    echo "Please specify at least PRODUCTION_BRANCH or RELEASE_PATTERN variable in Settins/Pipelines/Repository variables."
fi