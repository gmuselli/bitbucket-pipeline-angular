var SWPrecacheWebpackPlugin = require('sw-precache-webpack-plugin');


module.exports = {
    navigateFallback: '/index.html',
    navigateFallbackWhitelist: [/^(?!\/__)/], // <-- necessary for Firebase OAuth
    stripPrefix: 'dist/sample-ui',
    root: 'dist/sample-ui',
    maximumFileSizeToCacheInBytes: 10485760,
    plugins: [
        new SWPrecacheWebpackPlugin({
          cacheId: 'sample-ui',
          filename: 'service-worker.js',
          staticFileGlobs: [
            'dist/sample-ui/index.html',
            'dist/sample-ui/**.js',
            'dist/sample-ui/**.css'
          ],
          stripPrefix: 'dist/sample-ui/assets/', // stripPrefixMulti is also supported
          mergeStaticsConfig: true,
          maximumFileSizeToCacheInBytes: 10485760           
        })
      ]
  };
  