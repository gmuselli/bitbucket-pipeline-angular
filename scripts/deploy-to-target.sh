#!/bin/bash

# First configure aws
aws configure set region ${AWS_REGION}
aws configure set output json
aws configure set aws_secret_access_key ${AWS_SECRET_ACCESS_KEY}
aws configure set aws_access_key_id ${AWS_ACCESS_KEY} 

# Update with latest version (as we may have did a push before)
git pull

DEPLOYMENT_TARGET=$1

if [ -z "${DEPLOYMENT_TARGET}" ]; then
    echo "Please provide a deployment target corresponding to the public bucket in aws s3. Example: ${AWS_BUCKET_TEST_DEPLOY}"
    exit 1
fi

# sync with s3
aws s3 sync ./dist/sample-ui/ s3://${DEPLOYMENT_TARGET}/ --acl public-read