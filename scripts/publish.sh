#!/bin/bash

# First configure aws
aws configure set region ${AWS_REGION}
aws configure set output json
aws configure set aws_secret_access_key ${AWS_SECRET_ACCESS_KEY}
aws configure set aws_access_key_id ${AWS_ACCESS_KEY} 

# Update with latest version (as we may have did a push before)
git pull

# GET the version from the tag
VERSION=$(git describe --abbrev=0 --tags 2> /dev/null)

# Set DIRECTORY to the branch name
DIRECTORY=${BITBUCKET_BRANCH}

tar -czvf sample-ui.tar.gz -C dist/sample-ui .

aws s3 cp sample-ui.tar.gz s3://${AWS_BUCKET_ARTIFACT}/${DIRECTORY}/${VERSION}/sample-ui-${VERSION}.tar.gz