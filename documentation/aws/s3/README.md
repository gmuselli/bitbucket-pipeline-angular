# S3

## Prerequesite

- You need first to create an aws account. (if it is not the case go to [this page](https://aws.amazon.com/premiumsupport/knowledge-center/create-and-activate-aws-account/))
- Then go to [S3 Home](https://s3.console.aws.amazon.com/s3/home). S3 stands for Simple Storage Service. And the name as been well found. It helps you to store files on "buckets". It is really simple to configure. But you have as well advanced features like CORS configuration, access control or static web hosting.

## Create artefact bucket

You need first to create a bucket to store your artefcats. This bucket needs to be private as you don't want anyone (except you) to access to your history of version. 

You need first to click on "Create bucket"

![Create bucket](./S3_Create_Bucket.png)

Once you did it you will have to specify a name and sellect a region.

![Create bucket](./S3_Create_Bucket_1.png)

Once done you arrive on the permission configuration. 

![Create bucket](./S3_Create_Bucket_2.png)

By default the security is activated at all the steps. Any access are blocked and so accessible only to allowed user. You can have a look to the configuration but don't change anything. Click on next and go to "Create bucket"

## Create hosting buckets

We will need two bucket for web hosting. Follow the same steps as the artefact one but on the permission tab tab untick everything.

Then once your bucket is created select it and then click on properties.

![Web hosting](./S3_Properties.png)

Click on "Static website hosting" and click on "Use this bucket to host a website".

![Web hosting](./S3_Properties_1.png)

Fill the index of your page (here `index.html`) and click on save!

*You can see that the endpoint has been provided to access to your application* which contains nothing at the moment!