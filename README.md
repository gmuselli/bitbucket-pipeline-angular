# Angular application CI - CD with Bitbucket pipeline

## Introduction

This is a simple angular 8 application (created with `ng new`) which contains all the bitbucket-pipeline.yml steps for Continuous integration and Continuous Deployment using AWS S3 to store artefacts but as well to host the Angular web application.

## Prerquesite

### AWS

#### IAM

You need first to configure a group and a user with S3 Access and get the **AWS_SECRET_ACCESS_KEY** and **AWS_ACCESS_KEY**.

If you don't know how to do it you can follow the instructions [here](./documentation/aws/iam)

#### S3

You need then to create your buckets:

- One for test deployment
- One for prod deployment
- One for your artefacts (build results)

If you don't know how to do it you can follow the instructions [here](./documentation/aws/s3)

### Bitbucket

Now we are done. We can configure the environment variable for our pipeline.
To do this go to `Settings/Pipelines/Repository variables` and fill your variable. Be carreful to put at least the AWS_SECRET_ACCESS_KEY as **Secured**!

![Variable](./documentation/bitbucket/BB_Configure_Variables.png)

Here are the variable and their mean:

- AWS_ACCESS_KEY: this is the access key id you got once you created the user in iam section (it will be used to connect to AWS S3)
- AWS_ACCESS_SECRET_KEY: THIS MUST BE KEPT SECRET (use Secured). This is the secret access key id you got once you created the user in iam section (it will be used to connect to AWS S3)
- AWS_REGION: The region you selected for you S3 buckets
- AWS_BUCKET_ARTIFACT: The private bucket to store artifacts
- AWS_BUCKET_TEST_DEPLOY: The public bucket you created to deploy your bundle application in test environment
- AWS_BUCKET_PROD_DEPLOY: The public bucket you created to deploy your bundle application in prod environment
- PRODUCTION_BRANCH: This define the branch used for production releasing. You can as well use a pattern with variable RELEASE_PATTERN (like RELEASE_PATTERN=release/*). For more information you can check `Release cycle` section)

## The pipeline content

### Base image

The current pipeline is using image: `speedflyer/bitbucket-pipeline-aws-node-chrome` you can see the content of the Dockerfile [here](https://hub.docker.com/r/speedflyer/bitbucket-pipeline-aws-node-chrome/dockerfile). This is just an image that extends node:12.6.0. This adds as well the aws-cli and chrome to run tests. It then force USER to node to not run as root.

### Steps

At the moment it propose for all branches other than `develop` and `master`:

- Init: check variables and npm install using cache
- Test: run the test with ChromeHeadless

For `develop` and `master` additionnal steps are done:

- The build for production: Put your build logic here. You can differentiate them
- The release-preparation: Release preparation is doing handling the release cycle. To have a clear view of what it is doing you can go to the section **Release cycle**
- The publish: This will publish to your aws S3 artefact repository your build result. It will be stored in `<your_bucket>/<your_branch>/<version>/<name>-<version>.tar.gz`. Example: `sample-ui-artefacts/develop/develop-1.0.2-1/sample-ui-develop-1.0.2-1.tar.gz`
- The deploy-to-target: By specifying the targeted bucket you can specify to which stack you want to deploy

### Release cycle

It is possible to configure the release cycle. 

#### Default release cycle

By default it is configured for one production branch.
For example if you have a `develop` branch for your QA phase and `master` as a production branch you need to specify the variable `PRODUCTION_BRANCH=master`.

What will be done:

- If the branch is not the production branch, the npm version is not updated. But a tag will be created with format `<branch_name>-<npm version>-<bitbucket build number>`. Example: `develop-1.0.2-3`
- If the branch is the production branch, only the patch version will be increased. The npm version will be updated and a tag will be created with the npm version. If a minor or major change occures. It is up to the developer or release engineer to increase the minor/major manually on the development branch and then push this change to the production branch.

#### Multiple release branches

If working on several release branches it is possible to specify the releasing pattern with variable `RELEASE_PATTERN`. The pattern takes the following assumption:

- Release branches will be formatted with `<prefix><major>.<minor>`. Example: `release/1.0` or `production-2.4`. 
- The pattern must be explained in the following format: `<prefix>*`. Example: `release/*` or `production-*`. The `*` indicates where the version must be taken.

Two possible solutions:

- If the branch is not a release branch, the npm version is not updated. But a tag will be created with format `<branch_name>-<npm version>-<bitbucket build number>`. Example: `develop-1.0.2-3`
- If the branch is a release branch, the RELEASE_PATTERN will be computed. The version from the relase name is taken (example: `1.0`). 
    - If the version matches the `<major>.<minor>` of the npm version then only the patch version will be increased. The npm version will be updated and a tag will be created with the npm version
    - If it does not match, it means that it is the first build of this release. The npm version will be updated to the current release branch version with the patch `0` (example: `2.4.0` for `release/2.4` matching pattern `release/*`) and a tag will be created with the npm version.

> If you want to use multiple release branches don't forget to adapt your `bibucket-pipeline.yml` to use 

```
branches:
    release/*:
      - step: *init
      - ...
```

> A special note: if your branch name contains "/" or any other specific characters. They will be replaced by "_" as the tag name will be used to 

Here is a capture of your pipeline:

![Pipeline](./documentation/bitbucket/BB_Pipeline.png).

Here is an example with master branch releasing:

![Releasing](./documentation/bitbucket/BB_Commit_List.png).


You can follow you deployments in the deployment section. Here is an example: 

![Deployment](./documentation/bitbucket/BB_Deployment.png).
