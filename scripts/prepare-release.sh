#!/bin/bash

# This sanitize the tag by replacing special characters with _
function sanitizeTag() {
   echo "$1" | sed -e 's/[^-A-Za-z0-9\.\_]/_/g'
}

# This create a basic tag composed with ${BITBUCKET_BRANCH}-<${NPM_VERSION}>-<${BITBUCKET_BUILD_NUMBER}>
function simplyCreateTag() {
    echo "Simply create tag for ${BITBUCKET_BRANCH}"
    # in this is the first generated version
    NPM_VERSION=$(node -e "console.log(require('./package.json').version)")
    VERSION=$(sanitizeTag "${BITBUCKET_BRANCH}-${NPM_VERSION}-${BITBUCKET_BUILD_NUMBER}")
    git tag ${VERSION}
    git push origin ${VERSION}
}

# Create a release by modifying the npm version patch
function createRelease() {
    echo "Create new release tag for ${BITBUCKET_BRANCH}"
    # increment patch number. For all minor/major the update is manual by the developer or done by creating a new release branch matching RELEASE_PATTERN
    npm version patch --no-git-tag-version
    VERSION=$(node -e "console.log(require('./package.json').version)")
    git add --a
    # [skip ci] is keyword used by bitbucket pipeline. DONT REMOVE IT TO NOT ENTER IN INFINIT LOOP
    git commit -m "[skip ci] Bump to version ${VERSION}"
    git push
    git tag ${VERSION}
    git push origin ${VERSION}
}

# Compute releasing with production branch provided
# Taking following assumption:
# - If branch is the production branch the semantic is X.Y.Z and npm package must be updated
# - If branch is other the npm package is not updated and the semantic is ${BITBUCKET_BRANCH}-<NPM_VERSION>-<BITBUCKET_BUILD_NUMBER>
# - The version on release branch is always bumped to the patch number. If a major or a minor update must to be added developer can run: 
#       npm version (major | minor | patch | premajor | preminor | prepatch | prerelease)
#       git push
# => The build will be launched and the first patch number will be automatically created on this new minor or major
# 
# $1: PRODUCTION_BRANCH
#
function performBasicReleasing() {
    PRODUCTION_BRANCH=$1
    if [ "${BITBUCKET_BRANCH}" = "$PRODUCTION_BRANCH" ]; then
        createRelease
    else
        simplyCreateTag
    fi
}

# Compute releasing with production release pattern
# Taking following assumption:
# - Branch name is always <PATTERN><MAJOR>.<MINOR>. Example: release/1.0, production/2.4
# - If branch matches the pattern (example: release/1.0 for pattern release/*) then the version is updated to the latest patch
# - If branch does not match the pattern the npm package is not updated and the semantic is ${BITBUCKET_BRANCH}-<NPM_VERSION>-<BITBUCKET_BUILD_NUMBER>
# - The version on release branch is always bumped to the patch number. If a major or a minor update must to be added a new branch matching RELEASE_PATTERN must be created
# => The build will be launched and the first patch number will be automatically created on this new minor or major
# 
# $1: RELEASE_PATTERN
#
function performBranchReleasing() {
    RELEASE_PATTERN=$1

    if [[ "${BITBUCKET_BRANCH}" == $RELEASE_PATTERN ]]; then
        MAJOR_MINOR=${BITBUCKET_BRANCH: -3}
        echo "MAJOR.MINOR from release branch=${MAJOR_MINOR}"
        MAJOR_MINOR_PACKAGE_JSON=$(node -e "console.log(require('./package.json').version)")
        MAJOR_MINOR_PACKAGE_JSON=${MAJOR_MINOR_PACKAGE_JSON:0:3}
        echo "MAJOR.MINOR from package.json  =${MAJOR_MINOR_PACKAGE_JSON}"

        # In case the package.json MAJOR.MINOR matches the relase MAJOR.MINOR it means that it is not the first build
        if [ "${MAJOR_MINOR_PACKAGE_JSON}" = "${MAJOR_MINOR}" ]; then
            createRelease
        # This is the first build for the new release branch
        else 
            echo "Create first release tag for ${BITBUCKET_BRANCH}"
            # increment patch number. For all minor/major the update is manual by the developer
            npm version "${MAJOR_MINOR}.0" --no-git-tag-version
            VERSION=$(node -e "console.log(require('./package.json').version)")
            # [skip ci] is keyword used by bitbucket pipeline. DONT REMOVE IT TO NOT ENTER IN INFINIT LOOP
            git add --a
            git commit -m "[skip ci] Bump to version ${VERSION}"
            git push
            git tag ${VERSION}
            git push origin ${VERSION}
        fi
    else
        simplyCreateTag
    fi
}

# check if a relase pattern has been provided. If not get the 
if [ -z "${RELEASE_PATTERN}" ]; then
    performBasicReleasing "${PRODUCTION_BRANCH}"
else
    performBranchReleasing "${RELEASE_PATTERN}"
fi